import requests as rq
from typing import Self
import random
DEBUG = False

class WBot:
    words = [word.strip() for word in open("5letters.txt")]
    # print(words)
    w_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = w_url + "register"
    creat_url = w_url + "create"
    guess_url = w_url + "guess"

    def __init__(self: Self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(WBot.creat_url, json=creat_dict)

        self.choices = [w for w in WBot.words]
        random.shuffle(self.choices)

    def play(self: Self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WBot.guess_url, json=guess)
            rj = response.json()
            right = (rj["feedback"])
            status = "win" in rj["message"] 
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice} : {right}']
        # print(tries)
        if not won:
            for _ in range(5):
                if DEBUG:
                    print(choice, right, self.choices[:10])
                self.update(choice, right)
                if self.choices != []:
                    choice = random.choice(self.choices)
                    self.choices.remove(choice)
                    right, won = post(choice)
                if won:    
                    tries.append(f'{choice} : {right}')
                    break
                tries.append(f'{choice} : {right}')
            print('\n'.join(tries))
            if won and len(tries) < 7:
                print("THE WORD IS: ", choice, "found in", len(tries) , "attempts")
            else:
                print("WORD NOT FOUND WITHIN 6 TRIES")

    def update(self: Self, choice: str, right: str):
        for ind, (letter, check) in enumerate(zip(choice, right)):
            if check == 'G':
                self.choices = [w for w in self.choices if w[ind] == letter]
            elif check == 'Y':
                self.choices = [w for w in self.choices if letter in w and w[ind] != letter]
            else:
                self.choices = [w for w in self.choices if letter not in w]
        
game = WBot("CodeShifu")
game.play()


