def play(self) -> str:
    limit = 6
    count = 0

    while count < limit:
        count += 1
        if count == 1:
            choice = random.choice(self.choices)

        right, won = self.post_and_check(choice)

        print(f"Attempt {count}: {choice} => {right} correct")

        if won:
            print(f"Word is {choice} found in {count} attempts")
            return choice

        self.update(choice, right)

    print(f"Could not find the word in {limit} attempts")
    return None
