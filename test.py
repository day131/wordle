words = [word.strip() for word in open("5letters.txt")]

words = [word for word in words if word[2]=='e' and 's' in word and word[-1]!='s' and 'a' not in word and 'v' not in word and 'r' not in word]
print(words)
words = [word for word in words if word[0]=='s' and 'p' not in word and 'w' not in word and 'y' not in word]
print(words)
words = [word for word in words if 't' in word and 't' != word[-1] and 'u' not in word and 'n' not in word]
print(words)
words = [word for word in words if 't' != word[-2] and 'i' not in word and 'h' not in word]
print(words)